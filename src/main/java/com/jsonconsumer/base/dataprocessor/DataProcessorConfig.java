package com.jsonconsumer.base.dataprocessor;


import com.jsonconsumer.base.dataprocessor.application.DataProcessor;
import com.jsonconsumer.base.dataprocessor.incoming.DataProvider;
import com.jsonconsumer.base.dataprocessor.incoming.FileSaver;
import com.jsonconsumer.base.dataprovider.application.JsonPlaceHolderProvider;
import com.jsonconsumer.base.filecontentformatter.application.FileFormatStrategyProviderImpl;
import com.jsonconsumer.base.filesaver.application.FileSaverImpl;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
class DataProcessorConfig {

    @Bean
    DataProvider dataProvider(RestTemplateBuilder restTemplateBuilder) {
        return new JsonPlaceHolderProvider(restTemplateBuilder);
    }

    @Bean
    FileFormatStrategyProviderImpl fileFormatStrategyProvider() {
        return new FileFormatStrategyProviderImpl();
    }

    @Bean
    FileSaver fileSaver() {
        return new FileSaverImpl();
    }

    @Bean
    public DataProcessor dataProcessor(DataProvider dataProvider,
                                       FileFormatStrategyProviderImpl fileFormatStrategyProvider,
                                       FileSaver fileSaver) {
        return new DataProcessor(dataProvider, fileFormatStrategyProvider, fileSaver);
    }
}
