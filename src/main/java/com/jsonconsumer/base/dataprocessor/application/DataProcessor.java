package com.jsonconsumer.base.dataprocessor.application;

import com.jsonconsumer.base.dataprocessor.incoming.DataProvider;
import com.jsonconsumer.base.dataprocessor.incoming.FileSaver;
import com.jsonconsumer.base.filecontentformatter.application.FileFormatStrategyProviderImpl;
import com.jsonconsumer.base.filecontentformatter.application.formatstrategies.enums.FormatType;
import com.jsonconsumer.common.Post;

import java.util.Arrays;

public class DataProcessor {

    private final DataProvider dataProvider;
    private final FileSaver fileSaver;
    private final FileFormatStrategyProviderImpl formatProvider;

    public DataProcessor(DataProvider dataProvider,
                         FileFormatStrategyProviderImpl formatProvider,
                         FileSaver fileSaver) {
        this.dataProvider = dataProvider;
        this.formatProvider = formatProvider;
        this.fileSaver = fileSaver;
    }

    public void process() {
        Post[] data = dataProvider.getPostData();
        Arrays.stream(data).forEach(post ->
                fileSaver.writeToFile(post.getId(), post, formatProvider.provide(FormatType.JSON)));

    }
}
