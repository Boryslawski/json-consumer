package com.jsonconsumer.base.dataprocessor.incoming;

import com.jsonconsumer.common.Post;

public interface DataProvider {

    Post[] getPostData();
}
