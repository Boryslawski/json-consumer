package com.jsonconsumer.base.dataprocessor.incoming;

import com.jsonconsumer.base.filecontentformatter.application.FileFormatStrategy;

public interface FileSaver {

    void writeToFile(String fileName, Object data, FileFormatStrategy fileFormatStrategy);
}
