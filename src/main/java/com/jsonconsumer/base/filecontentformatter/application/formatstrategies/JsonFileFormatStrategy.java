package com.jsonconsumer.base.filecontentformatter.application.formatstrategies;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsonconsumer.base.filecontentformatter.application.FileFormatStrategy;
import com.jsonconsumer.base.filecontentformatter.application.formatstrategies.enums.FormatType;


class JsonFileFormatStrategy implements FileFormatStrategy {

    @Override
    public FormatType getType() {
        return FormatType.JSON;
    }

    @Override
    public String format(Object data) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(data);
    }
}
