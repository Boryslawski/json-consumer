package com.jsonconsumer.base.filecontentformatter.application.formatstrategies;

import com.jsonconsumer.base.filecontentformatter.application.FileFormatStrategy;
import com.jsonconsumer.base.filecontentformatter.application.formatstrategies.enums.FormatType;

class DefaultFileFormatStrategy implements FileFormatStrategy {

    @Override
    public FormatType getType() {
        return FormatType.DEFAULT;
    }

    @Override
    public String format(Object data) {
        return data.toString();
    }
}
