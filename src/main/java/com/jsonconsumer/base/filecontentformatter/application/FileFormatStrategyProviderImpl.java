package com.jsonconsumer.base.filecontentformatter.application;


import com.jsonconsumer.base.filecontentformatter.application.formatstrategies.enums.FormatType;

import javax.annotation.Resource;
import java.util.Map;

public class FileFormatStrategyProviderImpl {

    @Resource
    private Map<FormatType, FileFormatStrategy> strategiesMap;

    public FileFormatStrategy provide(FormatType type) {
        return strategiesMap.getOrDefault(type, strategiesMap.get(FormatType.DEFAULT));
    }
}
