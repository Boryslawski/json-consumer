package com.jsonconsumer.base.filecontentformatter.application.formatstrategies;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class StrategiesConfig {

    @Bean
    JsonFileFormatStrategy jsonFileFormatStrategy() {
        return new JsonFileFormatStrategy();
    }

    @Bean
    DefaultFileFormatStrategy defaultFileFormatStrategy() {
        return new DefaultFileFormatStrategy();
    }
}
