package com.jsonconsumer.base.filecontentformatter.application.formatstrategies.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum FormatType {
    JSON("json"),
    DEFAULT("txt");

    private final String extension;
}
