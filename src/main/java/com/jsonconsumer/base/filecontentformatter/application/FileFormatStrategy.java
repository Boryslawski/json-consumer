package com.jsonconsumer.base.filecontentformatter.application;

import com.jsonconsumer.base.filecontentformatter.application.formatstrategies.enums.FormatType;

import java.io.IOException;

public interface FileFormatStrategy {

    FormatType getType();

    String format(Object data) throws IOException;

    default String getExtension() {
        return getType().getExtension();
    }
}