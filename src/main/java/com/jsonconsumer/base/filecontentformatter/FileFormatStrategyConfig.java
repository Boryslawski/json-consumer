package com.jsonconsumer.base.filecontentformatter;

import com.jsonconsumer.base.filecontentformatter.application.FileFormatStrategy;
import com.jsonconsumer.base.filecontentformatter.application.formatstrategies.enums.FormatType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Configuration
class FileFormatStrategyConfig {

    @Autowired
    List<FileFormatStrategy> strategiesList;

    @Bean
    Map<FormatType, FileFormatStrategy> strategiesMap() {
        return strategiesList.stream()
                .collect(Collectors.toMap(FileFormatStrategy::getType, Function.identity()));
    }
}
