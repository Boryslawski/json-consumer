package com.jsonconsumer.base.dataprovider.application;

import com.jsonconsumer.base.dataprocessor.incoming.DataProvider;
import com.jsonconsumer.common.Post;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

public class JsonPlaceHolderProvider implements DataProvider {

    private static final String REST_URL = "https://jsonplaceholder.typicode.com/posts";
    private final RestTemplate restTemplate;

    public JsonPlaceHolderProvider(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public Post[] getPostData() {
        return restTemplate.getForObject(REST_URL, Post[].class);
    }
}
