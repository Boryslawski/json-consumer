package com.jsonconsumer.base.filesaver.application;

import com.jsonconsumer.base.dataprocessor.incoming.FileSaver;
import com.jsonconsumer.base.filecontentformatter.application.FileFormatStrategy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class FileSaverImpl implements FileSaver {

    private static final String BASE_PATH = "results";

    public void writeToFile(String fileName, Object data, FileFormatStrategy fileFormatStrategy) {
        String filePath = String.format("%s/%s.%s", BASE_PATH, fileName, fileFormatStrategy.getExtension());
        try {
            String fileContent = fileFormatStrategy.format(data);
            Files.writeString(Path.of(filePath), fileContent, StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
