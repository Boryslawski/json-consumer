package com.jsonconsumer.common;

import lombok.Data;

import java.io.Serializable;

@Data
public class Post implements Serializable {

    private String id;
    private String userId;
    private String title;
    private boolean completed;
}
