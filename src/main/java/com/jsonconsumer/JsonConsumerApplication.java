package com.jsonconsumer;

import com.jsonconsumer.base.dataprocessor.application.DataProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class JsonConsumerApplication {
    private static final Logger log = LoggerFactory.getLogger(JsonConsumerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(JsonConsumerApplication.class, args);
    }

    @Bean
    public CommandLineRunner run(DataProcessor dataProcessor) {
        log.info("Application started");
        return args -> {
            dataProcessor.process();
            System.exit(0);
        };
    }
}
